# QBO
# Dominik Ehnert
# 01.11.2018
# Increase 'nom = 443' by one for each new month in line 13
## Jeden Monat in Zeile 86:
## erste Dimension immer um 1 erhöhen
## qbo4.neu <- array(NA, c(419, 16))
#
library(ggplot2)

source("./filled.contour3.R")

nom = 443 # Number of months, increase by one for every new month.

fname = '../QBO/qbo_data/qbo.dat'       # 70 - 10 hPa fuer den Zeitraum 1953 - aktuell
fname2 = '../data/shea/qbo_u'		# 100 hPa fuer den Zeitraum 1957 - 1996
fname3 = '../data/qbo_100hPa.dat'       # 100 hPa seit 1997
fname4 = '../data/qbo.highres.dat'      # ab 1987


# 70 - 10 hPa fuer den Zeitraum 1953 - aktuell
line.n <- 1
for(i in 10:length(readLines(fname))){
  if(length((line=readLines(fname)[i]))>0){
    line.n <- line.n + 1
  }
}

qbo1 <- array(NA, c(line.n, 10))
colnames(qbo1) <- c("station", "YY", "MM", "70hPa", "50hPa", "40hPa", "30hPa", "20hPa", "15hPa", "10hPa")

for(i in 10:length(readLines(fname))){
  if(length((line=readLines(fname)[i]))>0){
    txt <- readLines(fname)[i]
    station <- as.numeric(paste(strsplit(txt,split="")[[1]][1:5], collapse=""))
    qbo1[i-9,1] <- station
    year <- as.numeric(paste(strsplit(txt,split="")[[1]][7:8], collapse=""))
    if(year > 50){
      year <- year+1900
    }else{
      year <- year+2000
    }
    qbo1[i-9,2] <- year
    mon <- as.numeric(paste(strsplit(txt,split="")[[1]][9:10], collapse=""))
    qbo1[i-9,3] <- mon
    p70 <- as.numeric(paste(strsplit(txt,split="")[[1]][13:16], collapse=""))
    qbo1[i-9,4] <- p70/10
    p50 <- as.numeric(paste(strsplit(txt,split="")[[1]][20:23], collapse=""))
    qbo1[i-9,5] <- p50/10
    p40 <- as.numeric(paste(strsplit(txt,split="")[[1]][27:30], collapse=""))
    qbo1[i-9,6] <- p40/10
    p30 <- as.numeric(paste(strsplit(txt,split="")[[1]][34:37], collapse=""))
    qbo1[i-9,7] <- p30/10
    p20 <- as.numeric(paste(strsplit(txt,split="")[[1]][41:44], collapse=""))
    qbo1[i-9,8] <- p20/10
    p15 <- as.numeric(paste(strsplit(txt,split="")[[1]][48:51], collapse=""))
    qbo1[i-9,9] <- p15/10
    p10 <- as.numeric(paste(strsplit(txt,split="")[[1]][55:58], collapse=""))
    qbo1[i-9,10] <- p10/10
  }
}


# 100 hPa fuer den Zeitraum 1957 - 1996
readLines(fname2)
data2 <- read.table(fname2)
qbo2 <- data2[, c(3,4,14)]/10
colnames(qbo2) <- c("YY", "MM", "100hPa")
for(i in 1:length(qbo2$YY)){
  if(qbo2$YY[i] > 50){
    qbo2$YY[i] <- qbo2$YY[i]+1900
  }else{
    qbo2$YY[i] <- qbo2$YY[i]+2000
  }
}


# 100 hPa seit 1997
data3 <- read.table(fname3)
qbo3 <- data3[1:3]
colnames(qbo3) <- c("YY", "MM", "100hPa")


# ab 1987
data4 <- read.table(fname4)
qbo4 <- data4[1:16]

qbo4.neu <- array(NA, c(nom, 16))    #erste Zahl immer um 1 erhöhen
colnames(qbo4.neu) <- c("YY", "MM", "10hPa", "12hPa", "15hPa", "20hPa", "25hPa", "30hPa", "35hPa", "40hPa", "45hPa", "50hPa", "60hPa", "70hPa", "80hPa", "90hPa")
dim(qbo4.neu)
dim(qbo4)
for(i in 1:16){
  qbo4.neu[,i] <- qbo4[,i]
}
lev.qbo <- c(10,12,15,20,25,30,35,40,45,50,60,70,80,90,100)

lev.qbo.sort <- sort(lev.qbo, index.return=TRUE)


### Zusammenfügen der vier Datensätze
qbo <- array(NA, c(line.n+72, 17))
colnames(qbo) <- c("YY", "MM", "100hPa", "90hPa", "80hPa", "70hPa", "60hPa", "50hPa", "45hPa", "40hPa", "35hPa", "30hPa", "25hPa", "20hPa", "15hPa", "12hPa", "10hPa")
qbo[,1] <- c(qbo1[,2], rep(NA,72))
qbo[,2] <- c(qbo1[,3], rep(NA,72))
qbo[,3] <- c(rep(NA,48), qbo2[,3], qbo3[,3], rep(NA,73))  # 100 hPa
qbo[,4] <- c(rep(NA,408), qbo4.neu[,16], rep(NA,73))      # 90 hPa
qbo[,5] <- c(rep(NA,408), qbo4.neu[,15], rep(NA,73))      # 80 hPa
qbo[,6] <- c(qbo1[1:408,4], qbo4.neu[,14], rep(NA,73))    # 70 hPa
qbo[,7] <- c(rep(NA,408), qbo4.neu[,13], rep(NA,73))      # 60 hPa
qbo[,8] <- c(qbo1[1:408,5], qbo4.neu[,12], rep(NA,73))    # 50 hPa
qbo[,9] <- c(rep(NA,408), qbo4.neu[,11], rep(NA,73))      # 45 hPa
qbo[,10] <- c(qbo1[1:408,6], qbo4.neu[,10], rep(NA,73))   # 40 hPa
qbo[,11] <- c(rep(NA,408), qbo4.neu[,9], rep(NA,73))      # 35 hPa
qbo[,12] <- c(qbo1[1:408,7], qbo4.neu[,8], rep(NA,73))    # 30 hPa
qbo[,13] <- c(rep(NA,408), qbo4.neu[,7], rep(NA,73))      # 25 hPa
qbo[,14] <- c(qbo1[1:408,8], qbo4.neu[,6], rep(NA,73))    # 20 hPa
qbo[,15] <- c(qbo1[1:408,9], qbo4.neu[,5], rep(NA,73))    # 15 hPa
qbo[,16] <- c(rep(NA,408), qbo4.neu[,4], rep(NA,73))      # 12 hPa
qbo[,17] <- c(qbo1[1:408,10], qbo4.neu[,3], rep(NA,73))   # 10 hPa

for(j in 1:408){
  qbo[j,7] <- mean(c(qbo[j,6], qbo[j,8]), na.rm=TRUE)
  qbo[j,9] <- mean(c(qbo[j,8], qbo[j,10]), na.rm=TRUE)
  qbo[j,11] <- mean(c(qbo[j,10], qbo[j,12]), na.rm=TRUE)
  qbo[j,13] <- mean(c(qbo[j,12], qbo[j,14]), na.rm=TRUE)
  if(j >= 37){
    qbo[j,16] <- mean(c(qbo[j,15], qbo[j,17]), na.rm=TRUE)
  }
  if(j >= 49){
    qbo[j,4] <- qbo[j,3]-0.33333*(qbo[j,3]-qbo[j,6])
    qbo[j,5] <- qbo[j,3]-0.66666*(qbo[j,3]-qbo[j,6])
  }
}



col.vector=c(-300,0,300) 
label.array <- rbind(c("1953", "1954", "1955", "1956", "1957", "1958", "1959", "1960", "1961"),
                     c("1962", "1963", "1964", "1965", "1966", "1967", "1968", "1969", "1970"),
                     c("1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979"),
                     c("1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988"),
                     c("1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997"),
                     c("1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006"),
                     c("2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015"),
                     c("2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024"))


pdf(file="./qbo_wind_pdf.pdf", width=5.8, height=8.3)
#jpeg(file="./qbo_wind.jpg", width=602, height=1000, units="px",quality = 100)
par(oma=c(1,1,1,1), mar=c(1,2,1,2), mfrow=c(8,1), mgp=c(3,0.3,0), las=1)
for(i in 1:8){
    filled.contour3(x=seq(1,108,1), y=rev(-log(lev.qbo.sort$x)), z=qbo[(1+(i-1)*108):(i*108),3:17], col=c("grey", "white"),
                    plot.axes={axis(1, at=seq(6,102,12), label=label.array[i,], tck=0, xlim=c(0,108))
                      axis(1, at=seq(0,108,1), label=NA, tck=0.02, lwd=0.5)
                      axis(3, at=seq(1,108,1), label=NA, tck=0.02, lwd=0.5)
                      axis(1, at=seq(12.5,108,12), label=NA, tck=1, lwd=0.6, lty="dashed", col="dimgrey")
                      axis(2, at=-log(c(100, 70, 50, 30, 20, 15, 10)), label=NA, tck=1, lwd=0.6, lty="dashed", col="dimgrey")
                      axis(2, at=-log(c(100, 70, 50, 30, 20, 10)), label=c(100, 70, 50, 30, 20, 10), tck=-0.01)
                      axis(4, at=-log(c(75, 42, 25, 14)), label=c(18, 22, 26, 30), tck=-0.01)})
    contour(x=seq(1,108,1), y=rev(-log(lev.qbo.sort$x)), z=qbo[(1+(i-1)*108):(i*108),3:17], levels=c(-30,-20,-10,10,20,30), lwd=0.5, labcex=0.5, method="flattest", add=TRUE)
    contour(x=seq(1,108,1), y=rev(-log(lev.qbo.sort$x)), z=qbo[(1+(i-1)*108):(i*108),3:17], levels=c(0), lwd=0.9, labcex=0.5, method="flattest", add=TRUE)
    par(xpd=NA)
    text(x=-5, y=-3.5, "hPa", cex=1, font=1.1, srt=90)
    text(x=114, y=-3.5, "km", cex=1, font=1.1, srt=-90)
    }
dev.off()

