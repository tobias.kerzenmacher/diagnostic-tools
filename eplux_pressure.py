#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
Tobias Kerzenmacher
Calculate diagnostic variables for QBOi data submission using idl code from Yaga Richter on the basis of Gerber and Manzini 2016: 
www.geosci-model-dev.net/9/3413/2016/
doi:10.5194/gmd-9-3413-2016
doi:10.5194/gmd-9-3413-2016-corrigendum
'''
import sys
import numpy as np
import scipy.integrate as integrate
import xarray as xr
# import aostools
# define constants
p0 = 101325 # Pa surface pressure;
Rd = 287.058 # J K-1 kg-1, gas constant for dry air
cp = 1004.64 # J K-1 kg-1, specific heat for dry air, at constant pressure
g0 = 9.80665 # m s-1, global average of gravity at mean sea level
a0 = 6371230 # m, Earth's radius
# Omega = 7.29212e-5 # s-1, Earth's rotation rate (2*np.pi/86400)
Omega = 2*np.pi/(24*3600.) # s-1, Earth's rotation rate (2*np.pi/86400)
# f = 2*Omega*np.sin(phi) # Coriolis parameter
kappa = Rd/cp
H = 7000.0 # Assume scale height 7000 m with  H = Rd*T_s/g0 this correspinds to T_s about 240 K, a constant reference air temperature.


def main(fname,exp):
    """Process EMAC data to calculate diagnostic variables for QBOi

    Arguments
    =========
    fname : string
            path and name of file to be processed.

    Returns
    =======
    -

    Raises
    ======
    SyntaxError : wrong path or file name
    SyntaxError : wrong time step
    """
    try:
        ds = xr.open_dataset(fname)
    except SyntaxError:
        print("Error path or file name")
        raise
    try:
        result = xr.merge((calculation(ds.isel(time=ix)).expand_dims('time',0) for ix in range(ds.time.size)))
    except SyntaxError:
        print("Merging failed because of time step issue")
        raise
    save_results(result,fname,exp)


def save_results(result,fname,exp):
    """Save diagnostic variables for QBOi into netcdf files

    Arguments
    =========
    result : xarray.Dataset 
             variables fy,fz,vstar,wstar,psistar,utenddivf

    Returns
    =======
    -

    Raises
    ======
    -
    """
    result.fy.attrs['long_name'] = 'northward EP-flux'
    result.fy.attrs['units'] = 'N * m^-1'
    result.fz.attrs['long_name'] = 'upward EP-flux'
    result.fz.attrs['units'] = 'N * m^-1'
    result.vstar.attrs['long_name'] = 'residual northward wind'
    result.vstar.attrs['units'] = 'm * s^-1'
    result.wstar.attrs['long_name'] = 'residual upward wind'
    result.wstar.attrs['units'] = 'm * s^-1'
    result.psistar.attrs['long_name'] = 'residual stream function'
    result.psistar.attrs['units'] = 'kg * s^-1'
    result.utenddivf.attrs['long_name'] = 'u-tendency by EP-flux divergence'
    result.utenddivf.attrs['units'] = 'm * s^-2'
    # result.to_netcdf(fname[:-3]+'_calc.nc')
 
    date = fname[88:96]
    print(date)
    result.fy.astype(np.float32).to_netcdf('fy_EMAC_QBOiExp'+exp+'_r1i1p1_'+date+'.nc')
    result.fz.astype(np.float32).to_netcdf('fz_EMAC_QBOiExp'+exp+'_r1i1p1_'+date+'.nc')
    result.vstar.astype(np.float32).to_netcdf('vstar_EMAC_QBOiExp'+exp+'_r1i1p1_'+date+'.nc')
    result.wstar.astype(np.float32).to_netcdf('wstar_EMAC_QBOiExp'+exp+'_r1i1p1_'+date+'.nc')
    result.psistar.astype(np.float32).to_netcdf('psistar_EMAC_QBOiExp'+exp+'_r1i1p1_'+date+'.nc')
    result.utenddivf.astype(np.float32).to_netcdf('utenddivf_EMAC_QBOiExp'+exp+'_r1i1p1_'+date+'.nc')
    

def pres2alt(p):
    """Calculate Altitude in meters.

    Arguments
    =========
    p : np.ndarray in Pa
        Pressure levels in Pascal.

    Returns
    =======
    z : np.ndarray
        High/Altitude in meters.

    Raises
    ======
    SyntaxError : Wrong Pressure levels
    """
    try:
        z = -np.log(p/p[-1])*H
    except SyntaxError:
        print("Error in pressure levels")
        raise

    return z


def T2theta(T,p):
    # Calculate Potential temperature
    theta = T*(p0/p)**kappa
    return theta


def lat2f(lat):
    # Calculate Coriolis parameter f from the latitudes in radians
    f = 2. * Omega * np.sin(lat)
    return f


def zonmean(x):
    # Calculate zonally averaged quantities of data array
    zx = x.mean('lon')
    return zx


def zonanomaly(x,xbar):
    # calculate zonal anomalies
    xp = x-xbar
    return xp


def dydp(fp,p):
    dydp = (np.gradient(fp,np.log(p),axis=0).T * 1/p.data).T
    return dydp


def omegares2wres(omegares,p):
    wres = -omegares*H/p # equation A13 in Gerber and Manzini 2016
    return wres


def calc_psi(vpthpbar,thzbar):
    psi = vpthpbar/thzbar
    return psi


def calc_acosin(phi):
    ac = a0*np.cos(phi)
    return ac


def calc_Fphi(ac,psi,dubardp,vpupbar):
    Fphi = ac*(dubardp*psi-vpupbar)
    return Fphi


def calc_Fp(ubar,phi,ac,f,vpthpbar,dthbardp,omegapupbar):
    temp = np.gradient((ubar*np.cos(phi)),phi,axis=1)
    Fp = (ac*f-temp)*vpthpbar/dthbardp-ac*omegapupbar
    return Fp


def calc_epdiv(Fphi,Fp,p,phi,ac):
    temp = np.gradient((Fphi.T*np.cos(phi)),phi,axis=1)
    Fphiphi = (temp/ac.data)
    Fpp = dydp(Fp,p)
    epdiv = Fphiphi+Fpp
    return epdiv


def calc_epfdiv(epdiv,ac):
    DELF = epdiv/ac.data*24.*3600.  # Eliassen-Palm flux divergence in m/s/d
    return DELF


def calc_streamf(psi,vbar,p,ac):
    Psi = np.zeros(np.shape(psi))
    Psi = integrate.cumtrapz(vbar,p,initial=0,axis=0)-psi
    Psi = 2*np.pi*ac.data/g0*Psi
    return Psi


def calc_omegares(omegabar,psi,phi,ac):
    temp = np.gradient(psi*np.cos(phi),phi,axis=1)
    omegares = omegabar + (1./ac)*temp
    return omegares


def calc_vres(vbar,psi,p):
    vres = vbar + np.gradient(psi,p,axis=0)
    return vres


def Fphi2Fphihat(Fphi,p):
    Fphihat = p/p0*Fphi
    return Fphihat


def Fp2Fzhat(Fp):
    Fzhat = -H/p0*Fp
    return Fzhat


def calc_zonmeanano(ds):
    omega = ds.vervel # omega vertical velocity in Pa/s
    th = T2theta(ds.tm1,ds.lev_2) # calculate potential temperature
    thbar = zonmean(th)
    ubar = zonmean(ds.um1)
    vbar = zonmean(ds.vm1)
    omegabar = zonmean(omega)
    # Calculate zonal anomalies
    up = zonanomaly(ds.um1,ubar)
    vp = zonanomaly(ds.vm1,vbar)
    omegap = zonanomaly(omega,omegabar)
    thp = zonanomaly(th,thbar)
    return ubar,up,vbar,vp,thbar,thp,omegabar,omegap


def calculation(ds):
    phi = np.radians(ds.lat) # convert degrees to radians    
    f = lat2f(phi) # Calculate Coriolis parameter

    # calculate zonally Averaged quantities and zonal anomalies
    ubar,up,vbar,vp,thbar,thp,omegabar,omegap = calc_zonmeanano(ds)

    # calculate vertical derivatives in pressure coordinates for omega
    ubardp = dydp(ubar,ds.lev_2)
    thbardp = dydp(thbar,ds.lev_2)

    # calculate zonally averaged quantities
    vpthpbar = zonmean(vp*thp) 
    vpupbar = zonmean(vp*up)
    omegapupbar = zonmean(up*omegap)  # this is omega'*u'

    # for omega (dp/dt)
    ac = calc_acosin(phi)
    psi = calc_psi(vpthpbar,thbardp) # eddy stream function
    Fphi = calc_Fphi(ac,psi,ubardp,vpupbar) # Eliassen-Palm flux northward component
    Fp = calc_Fp(ubar,phi,ac.data,f.data,vpthpbar,thbardp,omegapupbar) # Eliassen-Palm flux vertical component
    epdiv = calc_epdiv(Fphi,Fp,ds.lev_2,phi,ac) # Eliassen-Palm divergence
    DELF = calc_epfdiv(epdiv,ac) # Eliassen-Palm flux divergence in m/s/d
    Psi = calc_streamf(psi,vbar,ds.lev_2,ac) # mass stream function in kg/s at p
    vres = calc_vres(vbar,psi,ds.lev_2) # transformed Euler mean northward velocity
    omegares = calc_omegares(omegabar,psi,phi,ac.data) # transformed Euler mean vertical velocity
    wres = omegares2wres(omegares,ds.lev_2)
    
    # transform to log-pressure coordinate
    Fphihat = Fphi2Fphihat(Fphi,ds.lev_2)
    Fzhat = Fp2Fzhat(Fp)

    Fphihat.name = 'fy' # northward component of Eliassen-Palm flux
    Fzhat.name = 'fz' # upward component of Eliassen-Palm flux
    vres.name = 'vstar' # residual mean northward wind
    wres.name = 'wstar' # residual mean upward wind
    Psi.name = 'psistar' # mean mass stream function
    utenddivf = xr.DataArray(DELF, coords=[vres.lev_2, vres.lat], dims=['lev_2', 'lat']) # tendency of eastward wind due to Eliassen-Palm flux divergence
    utenddivf.name = 'utenddivf'
    result = xr.merge([Fphihat,Fzhat,vres,wres,Psi,utenddivf])
    return result
    # end of program
  

if __name__ == '__main__':
    fname = str(sys.argv[1]) # '/work/kit/imk-asf/px5501/qboi_exp1/IMK_MESSy______20141101_0000_6h02_pl.nc'
    exp = str(sys.argv[2]) 
    # fname = '/Users/tobias/IMK_MESSy______19790101_0600_6h02_pl.nc'
    main(fname,exp)
