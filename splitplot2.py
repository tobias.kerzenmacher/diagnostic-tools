import matplotlib.pyplot as plt
#from matplotlib.mlab import griddata
from matplotlib import dates
from matplotlib.ticker import ScalarFormatter,FormatStrFormatter,NullFormatter
import matplotlib as mpl
mpl.use('agg')
import numpy as np


def splitplot(fds,u,pressure,nrows,fname):
    altitude = -7*np.log(np.array(pressure)/1013.15)
    deltat = int((np.nanmax(fds)-fds[0])/nrows)+1
    alticks = np.linspace(np.around(altitude[0]),np.around(altitude[-1]),num=9)
    mpl.rc('figure', figsize=(8.27,11.69))  # A4 portrait
    contour_levels = range(-40, 45, 5)
    fig, ax1 = plt.subplots(nrows=nrows)

    for j in range(nrows):
        ax = plt.subplot(nrows, 1, j + 1)
        
        by0 = fds[0]+j*deltat
        by1 = fds[0]+(j+1)*deltat

        axt = fds[np.nanargmin(abs(fds-by0)):1+np.nanargmin(abs(fds-by1))]
        axz = u[:,np.nanargmin(abs(fds-by0)):1+np.nanargmin(abs(fds-by1))]
    
        sm1 = ax.contourf(axt,pressure,axz,contour_levels,cmap='BrBG_r',extend='both')

        ax.set_yscale('log')
        ax.invert_yaxis()
        ax.get_yaxis().set_major_formatter(FormatStrFormatter('%1.f'))
        ax.get_yaxis().set_minor_formatter(FormatStrFormatter('%1.f'))
        ax.yaxis.set_minor_formatter(NullFormatter())

        ax.set_yticks([100,70,40,20,10])
        ax.set_ylim([pressure[0],pressure[-1]])

        years = dates.YearLocator()   # every year
        months = dates.MonthLocator()   # every month
        yearsFmt = mpl.dates.DateFormatter('%Y')
        ax.xaxis.set_major_formatter(yearsFmt)
        ax.xaxis.set_major_locator(years)
        ax.xaxis.set_minor_locator(months)

        ax2 = plt.twinx()
        ax2.contour(axt,altitude,axz, levels=[-40,-30,-20,-10,0,10,20,30,40], colors=('k',),extend='both')

        ax2.get_yaxis().set_major_formatter(ScalarFormatter())
        ax2.set_yticks(alticks)
        ax2.set_ylim([altitude[0],altitude[-1]])

    
        cax = fig.add_axes([0.2, 0.04, 0.6, 0.01])
        cb1 = fig.colorbar(sm1,cax=cax,orientation='horizontal')

    plt.text(0.07, -.54,'E', horizontalalignment='center',
             verticalalignment='center',
             transform=ax.transAxes,size=15)
    plt.text(0.90, -.54,'W', horizontalalignment='center',
             verticalalignment='center',
             transform=ax.transAxes,size=15)
    plt.text(-0.10, nrows*0.55,'pressure [hPa]', horizontalalignment='center',
             verticalalignment='center',
             transform=ax.transAxes,size=15,rotation=90)
    plt.text(1.08, nrows*0.55,'height [km]', horizontalalignment='center',
             verticalalignment='center',
             transform=ax.transAxes,size=15,rotation=90)
    cb1.ax.get_xaxis().labelpad = -1
    cb1.set_label('u [m/s]', size=15)
    ax.set_xlabel('time [year]', size=15)

    plt.savefig(fname, format='pdf')
    plt.show()
