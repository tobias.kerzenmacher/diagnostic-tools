#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
Read QBO data from the following files qbo.dat and singapore.dat and plot
'''

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
#from matplotlib.mlab import griddata
from matplotlib import dates
from matplotlib.ticker import ScalarFormatter,FormatStrFormatter,NullFormatter
import datetime
from requests import get  # to make GET request
from scipy.interpolate import griddata
from urllib.request import urlopen
import io



def download(url, file_name):
    # open in binary mode
    with open(file_name, "wb") as file:
        # get request
        response = get(url)
        # write to file
        file.write(response.content)


def read_singapore(nmonth,nyear):
    headerlines = []
    ye = []
    dats = []
    count = 0
    date = []
    url = "https://www.atmohub.kit.edu/data/singapore.dat"
    #download(url,"Data/singapore.dat")
    with urlopen(url) as file:
        for i in range(3):
            headerlines.append(file.readline().decode().strip())
        for year in range(1987,nyear+1):
            if year == nyear:
                data = np.zeros([nmonth,15])*np.nan
            else:
                data = np.zeros([12,15])*np.nan
            ye.append(file.readline().strip())
            file.readline()
            if (year < 1997):
                data = np.zeros([12,15])*np.nan
                for i in range(14):
                    cols = file.readline()
                    cols = cols.strip().split()
                    for j in range(1,13):
                        if i == 1:
                            date.append(datetime.datetime(year, j, 1))
                        data[j-1,i] = float(cols[j])
            else:
                for i in range(15):
                    cols = file.readline()
                    cols = cols.strip().split()
                    for j in range(1,13):
                        if j < nmonth+1 or year < nyear:
                            if i == 1:
                                date.append(datetime.datetime(year, j, 1))
                            data[j-1,i] = float(cols[j])
            dats.extend([list(i) for i in data])
            file.readline()
            count = count + 1
    pressure = [100, 90, 80, 70, 60, 50, 45, 40, 35, 30, 25, 20, 15, 12, 10]
    altitude = -7*np.log(np.array(pressure)/1013.15)
    fds = list(mpl.dates.date2num(date))
    fds = np.array(fds)
    return np.array(dats).T[::-1],fds,pressure,altitude


def read_qbo():
    url = "https://www.atmohub.kit.edu/data/qbo.dat"

    with urlopen(url) as response:
        content = response.read().decode('utf-8')

    data = np.genfromtxt(io.StringIO(content), skip_header=9, dtype=['S6','S4','i4','i1','i4','i1','i4','i1','i4','i1','i4','i1','i4','i1','i4','i1'], names=['station','date','p70','n70','p50','n50','p40','n40','p30','n30','p20','n20','p15','n15','p10','n10'], delimiter=[6,4,6,2,5,2,5,2,5,2,5,2,5,2,5,2], filling_values=-999999, missing_values=' ')


    #url = "https://www.atmohub.kit.edu/data/qbo.dat"
    ##download(url,"singapore_winds_1953-2017_qbo.dat")
    #data = np.genfromtxt('/Users/tobias/Git/QBOSingapore/srv/qbo.dat',skip_header=9,dtype=['S6','S4','i4','i1','i4','i1','i4','i1','i4','i1','i4','i1','i4','i1','i4','i1'],names=['station','date','p70','n70','p50','n50','p40','n40','p30','n30','p20','n20','p15','n15','p10','n10'],delimiter=[6,4,6,2,5,2,5,2,5,2,5,2,5,2,5,2],filling_values=-999999,missing_values=' ')#
    date = []
    dlen = 0
    for i in range(len(data)):
        # print(data['date'][i])
        if data['date'][i]:
            dlen = dlen + 1   
            if int(data['date'][i]) > 5000:
                date.append(datetime.datetime.strptime('19'+(data['date'][i]).decode('UTF-8'),'%Y%m'))
            else:
                date.append(datetime.datetime.strptime('20'+(data['date'][i]).decode('UTF-8'),'%Y%m'))

    fds = mpl.dates.date2num(date)
#
    up = np.array(list([data['p70'],data['p50'],data['p40'],data['p30'],data['p20'],data['p15'],data['p10']]))

    pressure = [70,50,40,30,20,15,10]
    altitude = -7*np.log(np.array(pressure)/1013.15)
#    print(up)
    return up,fds,pressure,altitude

    
def splitplot(fds,u,pressure,nrows,fname):
    # print(fds[0],fds[-1])
    altitude = -7*np.log(np.array(pressure)/1013.15)
    deltat = int((np.nanmax(fds)-fds[0])/nrows)+1
    alticks = np.linspace(np.around(altitude[0]),np.around(altitude[-1]),num=9)
    print(alticks)
    mpl.rc('figure', figsize=(8.27,11.69))  # A4 portrait
    contour_levels = range(-40, 45, 5)
    fig, ax1 = plt.subplots(nrows=nrows)

    for j in range(nrows):
        ax = plt.subplot(nrows, 1, j + 1)
        # ax = ax1[j]
        
        by0 = fds[0]+j*deltat
        by1 = fds[0]+(j+1)*deltat

        axt = fds[np.nanargmin(abs(fds-by0)):1+np.nanargmin(abs(fds-by1))]
        axz = u[:,np.nanargmin(abs(fds-by0)):1+np.nanargmin(abs(fds-by1))]

        #         print(by0,by1)
        # if j == nrows-1:
        #     axt = np.append(fds[np.nanargmin(abs(fds-by0)):1+np.nanargmin(abs(fds-by1))],by1)
        #     axz = np.append(u[:,np.nanargmin(abs(fds-by0)):1+np.nanargmin(abs(fds-by1))], np.zeros([np.shape(u)[0],1])*np.nan, axis=1)
        # else:
        #     axt = fds[np.nanargmin(abs(fds-by0)):np.nanargmin(abs(fds-by1))]
        #     axz = u[:,np.nanargmin(abs(fds-by0)):np.nanargmin(abs(fds-by1))]

        # print(np.shape(axt), np.shape(axz))
    
        sm1 = ax.contourf(axt,pressure,axz,contour_levels,cmap='BrBG_r',extend='both')

        ax.set_yscale('log')
        ax.invert_yaxis()
        # ax.get_yaxis().set_major_formatter(ScalarFormatter('%1.1f'))
        ax.get_yaxis().set_major_formatter(FormatStrFormatter('%1.f'))
        ax.get_yaxis().set_minor_formatter(FormatStrFormatter('%1.f'))
        ax.yaxis.set_minor_formatter(NullFormatter())

        ax.set_yticks([100,70,40,20,10])
        # ax.set_ylim(plt.ylim()[::-1])
        ax.set_ylim([pressure[0],pressure[-1]])

        years = dates.YearLocator()   # every year
        months = dates.MonthLocator()   # every month
        yearsFmt = mpl.dates.DateFormatter('%Y')
        ax.xaxis.set_major_formatter(yearsFmt)
        ax.xaxis.set_major_locator(years)
        ax.xaxis.set_minor_locator(months)
        # ax.xaxis.set_minor_locator(AutoMinorLocator(12))

        ax2 = plt.twinx()
        ax2.contour(axt,altitude,axz, levels=[-40,-30,-20,-10,0,10,20,30,40], colors=('k',),extend='both')

        ax2.get_yaxis().set_major_formatter(ScalarFormatter())
        # ax2.set_yticks([18,20,22,24,26,28,30,32])
        ax2.set_yticks(alticks)
        ax2.set_ylim([altitude[0],altitude[-1]])

    
        # cax = fig.add_axes([0.3, 0.95, 0.4, 0.02])
        #                  [x  , y   , xlänge,ydicke]                    
        cax = fig.add_axes([0.2, 0.04, 0.6, 0.01])
        cb1 = fig.colorbar(sm1,cax=cax,orientation='horizontal')

    # plt.text(0.19, nrows*1.22,'E', horizontalalignment='center',
    #          verticalalignment='center',
    #          transform=ax.transAxes,size=15)
    # plt.text(0.78, nrows*1.22,'W', horizontalalignment='center',
    #          verticalalignment='center',
    #          transform=ax.transAxes,size=15)
    plt.text(0.07, -.54,'E', horizontalalignment='center',
             verticalalignment='center',
             transform=ax.transAxes,size=15)
    plt.text(0.90, -.54,'W', horizontalalignment='center',
             verticalalignment='center',
             transform=ax.transAxes,size=15)
    plt.text(-0.10, nrows*0.55,'pressure [hPa]', horizontalalignment='center',
             verticalalignment='center',
             transform=ax.transAxes,size=15,rotation=90)
    plt.text(1.08, nrows*0.55,'height [km]', horizontalalignment='center',
             verticalalignment='center',
             transform=ax.transAxes,size=15,rotation=90)
    cb1.ax.get_xaxis().labelpad = -1
    cb1.set_label('u [m/s]', size=15)
    ax.set_xlabel('time [year]', size=15)

    plt.savefig(fname, format='pdf')
    plt.show()

# ----definitions end
#
# ----main program starts here


def main():
    # -------------READ----------------------------------------------
    up1,fds1,pressure1,altitude1 = read_qbo()
    tnmoth = np.shape(up1)[1] # total number of months
    up = np.zeros([15,tnmoth])*np.nan
    nmonth = np.shape(up1)[1]%12 # number of months above a whole year
    nyear = mpl.dates.num2date(fds1[-1]).year
    print(nmonth,nyear)
    up2,fds2,pressure,altitude = read_singapore(nmonth,nyear)
    fds = fds1
    fds[-fds2.size:] = fds2
    fds = np.array(fds)
    print(fds[-1])
    up1 = up1*1.
    up1[up1 < -10000] = np.nan
    up[3,:] = up1[0,:]
    up[5,:] = up1[1,:]
    up[7,:] = up1[2,:]
    up[9,:] = up1[3,:]
    up[11,:] = up1[4,]
    up[12,:] = up1[5,:]
    up[14,:] = up1[6,:]
    up[-up2.shape[0]:,-up2.shape[1]:] = up2
    ind = ~np.isnan(up)
    time,press = np.meshgrid(fds,pressure)
    uu = np.array(up[ind])*.1
    time = time[ind]
    press = press[ind]
    
    #u = griddata(time,press,uu,fds,pressure,interp='linear')
    #print(np.shape(time), np.shape(press), np.shape(uu))
    #print(np.shape(fds), np.shape(pressure))
    points1 = np.array([[time[i],press[i]] for i in range(len(time))])
    #print(points1.shape)
    X, Y = np.meshgrid(fds,pressure)
    u = griddata(points1, uu, (X,Y), method='linear')
    nrows = 7
    fname = 'singapore_QBO_' + str(nyear) + str(nmonth).zfill(2)+'.pdf'
    splitplot(fds,u,pressure,nrows,fname)
    
    
if __name__ == "__main__":
    main()
