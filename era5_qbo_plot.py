#!/usr/bin/python3
# -*- coding: utf-8 -*-
import matplotlib as mpl
mpl.use('agg')
from matplotlib import dates
import glob
#from fub_qbo_plot4new import splitplot
from splitplot2 import splitplot
import xarray as xr
import pandas as pd
import numpy as np


def read_netcdfs_alt(files, dim, transform_func=None):
    def process_one_path(path):
        # use a context manager, to ensure the file gets closed after use
        with xr.open_dataset(path) as ds:
            # transform_func should do some sort of selection or
            # aggregation
            if transform_func is not None:
                ds = transform_func(ds)
            # load all data from the transformed dataset, to ensure we can
            # use it after closing each original file
            ds.load()
            return ds
    print(files)
    paths = sorted(glob.glob(files))
    print(paths)
    datasets = [process_one_path(p) for p in paths]
    combined = xr.concat(datasets, dim)
    return combined


def readeranew():
    # here we suppose we only care about the combined mean of each file;
    # you might also use indexing operations like .sel to subset datasets
    path = '/Users/tobias/Mount/Lsdf/Ecmwf/Era5/'
    fname = path+'era5_mm_go3tuv????.nc'
    fname = path+'era5_mm_u_????_zm.nc'
    # fname = path+'era-int_pl_????????.nc'
    # fname = glob.glob(path+'era-int_pl_2016????.nc')
    ds = read_netcdfs_alt(fname,
                          dim='time',
                          transform_func = lambda ds: ds.sel(lat=slice(3,-3)).mean(['lat','lon']))
    # ds = ds.resample("1MS", dim='time',how='mean')
    return ds


# ----definitions end
#
# ----main program starts here
mode = 'write'
if mode == 'read':
    ds = readeranew()
    ds.to_netcdf('qbo_era_index_202112.nc')
else:
    with xr.open_dataset('qbo_era_index_202112.nc') as ds:
        ds.load()

print(ds)
dss = ds.sel(level=slice(10, 100))#.resample(time='1MS').mean()#.sel(latitude=slice(3,-3)).mean('latitude')
# dss = ds.resample("1MS", dim='time',how='mean')#.sel(latitude=slice(3,-3)).mean('latitude')
print(dss)
dsgb = dss.groupby('time.month')
climatology = dsgb.mean('time')
anomalies = dsgb - climatology
# fds = [dates.date2num(i) for i in pd.to_datetime(ds.time.data)]
# miny = ds.time[0].values.astype('datetime64[Y]').astype(int) + 1970
# maxy = ds.time[-1].values.astype('datetime64[Y]').astype(int) + 1970
fname = 'era_202112_QBO.pdf'
fds = np.array([dates.date2num(i) for i in pd.to_datetime(anomalies.time.data)])
pressure = anomalies.level.data
uver = anomalies.u.data
splitplot(fds,uver.T[::-1],pressure[::-1],7,fname)
